<?php

/**
 * @file
 * Slack integration module admin functions.
 */

/**
 * Slack module admin form.
 */
function slack_node_update_configure_form($form, &$form_state) {
  $form['slack_node_update_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select which Content-Types should push an update'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('slack_node_update_content_types', array()),
  );
  $form['slack_node_update_channel'] = array(
    '#type' => 'textfield',
    '#title' => t('Default channel'),
    '#description' => t('Enter your channel name with # symbol, for example #general (or @username for a private message or a private group name).'),
    '#default_value' => variable_get('slack_channel'),
  );
  $form['slack_node_update_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Default username'),
    '#description' => t('What would you like to name your Slack bot?'),
    '#default_value' => variable_get('slack_node_update_username'),
  );
  return system_settings_form($form);
}
